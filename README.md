# cfn-build

CloudFormation build utilities

**If this is your first time visiting this project, check jhctechnology/aws-cloudformation-utilities/awslint> first for an introduction and contribution guidelines.**

## Prerequisites
1. awscli

## Using the project
We recommend including this project as part of the [awslint project](https://code.chs.usgs.gov/chs-library/docker/awslint/). This project and its scripts are built into the docker image provided by awslint and can be called directly from the command line as they are fetched from the path.

## Scripts
The following scripts are defined in this project. They are classified as:
* **Type:** CI: this script is primarily used to execute CI jobs
* **Type:** Utility: this script is used by one or multiple CI or Utility scripts as a way to reuse and modularize code segments
* **Type:** CI/Utility: this script defines a function which can be used as a utility by other scripts and also has use cases as a standalone CI job

### [cleanup_failed_change_sets.sh](cleanup_failed_change_sets.sh)
Looks for change sets on the stack with the same stack name. If they exist and are in the failed state, we can safely delete them to proceed. If they are in any other state, we will preserve them to avoid conflicts.

**Type:** CI
#### Required Parameters
* CFN_STACK_NAME
* CFN_CHANGE_SET_NAME

### [package_lambda_simple.sh](package_lambda_simple.sh)
Simple Lambda function packager; zips the entire contents of a directory. Recommended use as a build step with artifacts made available as GitLab CI artifacts.

**Type:** CI
#### Required Command Line Arguments
1. relative or absolute path of the parent folder under which Lambda script artifacts are contained

##### Directory Structure
```
build_root/
  lambda/folder/ <-- this is the path you pass to the script
    my-lambda-1
      myCode.js
    my-lambda-2
      myScript.py
```

##### Output
```
build_root/
  lambda/folder/
    my-lambda-1.zip
    my-lambda-2.zip
    my-lambda-1
      myCode.js
    my-lambda-2
      myScript.py
```

### [stage_templates_s3.sh](stage_templates_s3.sh)
Uploads files to an S3 bucket based on the arguments passed. Creates the bucket if it does not exist.

**Type:** CI
#### Required Command Line Arguments
1. S3 bucket name for staging the files
2. Key prefix
3. A list of local paths to upload to S3

#### Recommended Uses
1. Stage S3 files in a temporary location to perform `validate-template` and evaluate describe-only change sets. This allows the use of the `--template-url` option instead of `--template-body` which has limitations as to file size.

```bash
./$CFN_BUILD_PATH/stage_templates_s3.sh $CI_PROJECT_NAME-temp-bucket $CFN_STAGING_S3_PREFIX cloudformation/myFile1.yaml cloudformation/myFile2.yaml
```

2. Stage S3 files in a permanent location after performing applicable testing.

```bash
./$CFN_BUILD_PATH/stage_templates_s3.sh $CFN_STAGING_BUCKET $CFN_STAGING_S3_PREFIX cloudformation/myFile1.yaml cloudformation/myFile2.yaml
```
