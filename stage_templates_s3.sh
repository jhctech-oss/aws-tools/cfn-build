#!/bin/bash -e

BUCKET_NAME=$(echo "$1" | tr '[:upper:]' '[:lower:]');
PREFIX="$2";
# Drop the first two arguments
shift;
shift;
TEMPLATE_PATHS="$@"; # all remaining command line arguments

if ! aws s3api head-bucket --bucket "$BUCKET_NAME" 2>/dev/null; then
  echo "$BUCKET_NAME does not exist. Creating..."
  aws s3 mb s3://$BUCKET_NAME
fi

echo "Uploading files from paths $TEMPLATE_PATHS to s3://$BUCKET_NAME..."
for path in $TEMPLATE_PATHS
do
  if [ -d "$path" ]
  then
    echo "aws s3 sync $path/ s3://${BUCKET_NAME}/${PREFIX} --delete"
    aws s3 sync $path/ s3://${BUCKET_NAME}/${PREFIX} --delete
  else
    echo "aws s3 sync $path s3://${BUCKET_NAME}/${PREFIX}${path} --delete"
    aws s3 sync $path s3://${BUCKET_NAME}/${PREFIX}${path} --delete
  fi
done
